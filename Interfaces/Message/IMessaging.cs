﻿// ***********************************************************************
// <copyright file="IMessaging.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace SoulNetLib.Interfaces.Message
{
    /// <summary>
    /// Interface IMessaging
    /// </summary>
    public interface IMessaging
    {
        /// <summary>
        /// Sends the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>Task.</returns>
        void Send(byte[] data);
    }
}