﻿// ***********************************************************************
// <copyright file="IMessage.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace SoulNetLib.Interfaces.Message
{
    /// <summary>
    /// Interface IMessage
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        int Id { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        byte[] Data { get; set; }
    }
}