﻿// ***********************************************************************
// <copyright file="IConnection.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using SoulNetLib.Common;
using SoulNetLib.Interfaces.Protocol;

namespace SoulNetLib.Interfaces.Connection
{
    /// <summary>
    /// Interface IConnection
    /// </summary>
    public interface ISoulConnection : IConnectible, IDisposable
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        int Id { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ISoulConnection"/> is connected.
        /// </summary>
        /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
        bool Connected { get; }

        /// <summary>
        /// Gets or sets the protocol for incoming data.
        /// </summary>
        /// <value>The protocol.</value>
        ISoulProtocol Protocol { get; set; }
    }

    /// <summary>
    /// Enum ConnectionState
    /// </summary>
    public enum ConnectionState
    {
        Disconnected = 0,
        Disconnecting = 1,
        Connecting = 2,
        Connected = 3,
        Disposed = 4,
    }
}