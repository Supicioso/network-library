﻿// ***********************************************************************
// <copyright file="IConnectible.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Net.Sockets;

namespace SoulNetLib.Interfaces.Connection
{
    /// <summary>
    /// Interface IConnectible
    /// </summary>
    public interface IConnectible
    {
        /// <summary>
        /// Gets or sets the client.
        /// </summary>
        /// <value>The client.</value>
        Socket Socket { get; set; }

        /// <summary>
        /// Gets the current connection state.
        /// </summary>
        /// <value>The state.</value>
        ConnectionState State { get; }
    }
}