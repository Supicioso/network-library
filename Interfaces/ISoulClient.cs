﻿// ***********************************************************************
// <copyright file="ISoulClient.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using SoulNetLib.Interfaces.Connection;
using SoulNetLib.Interfaces.Message;

namespace SoulNetLib.Interfaces
{
    /// <summary>
    /// Interface ISoulClient
    /// </summary>
    public interface ISoulClient : ISoulConnection, IMessaging
    {
         
    }
}