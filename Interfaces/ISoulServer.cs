﻿// ***********************************************************************
// <copyright file="ISoulServer.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using SoulNetLib.Containers;
using SoulNetLib.Interfaces.Connection;

namespace SoulNetLib.Interfaces
{
    /// <summary>
    /// Interface ISoulServer
    /// </summary>
    public interface ISoulServer : ISoulConnection, IDisposable
    {
        /// <summary>
        /// Gets or sets the connections.
        /// </summary>
        /// <value>The connections.</value>
        SoulConnections Connections { get; set; }

        /// <summary>
        /// Gets or sets the listen port.
        /// </summary>
        /// <value>The listen port.</value>
        int ListenPort { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ISoulServer"/> is listening.
        /// </summary>
        /// <value><c>true</c> if listening; otherwise, <c>false</c>.</value>
        bool Listening { get; set; }
    }
}