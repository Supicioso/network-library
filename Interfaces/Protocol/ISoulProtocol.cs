﻿// ***********************************************************************
// <copyright file="ISoulProtocol.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using SoulNetLib.Interfaces.Message;
using SoulNetLib.Network.Protocol;

namespace SoulNetLib.Interfaces.Protocol
{
    /// <summary>
    /// Interface ISoulProtocol
    /// </summary>
    public interface ISoulProtocol
    {
        /// <summary>
        /// Creates a message from incoming data.
        /// </summary>
        /// <param name="receivedBytes">The received bytes.</param>
        /// <returns>The message</returns>
        Task<List<IMessage>> CreateMessage(byte[] receivedBytes);

        /// <summary>
        /// Gets or sets the size of the receive buffer.
        /// </summary>
        /// <value>The size of the receive buffer.</value>
        [DefaultValue(4096)]
        int ReceiveBufferSize { get; set; }

        /// <summary>
        /// Gets or sets the packet handler.
        /// </summary>
        /// <value>The packet handler.</value>
        ISoulPacketHandler PacketHandler { get; set; }

        /// <summary>
        /// Gets or sets the protocol version.
        /// </summary>
        /// <value>The version.</value>
        ProtocolVersion Version { get; set; }
    }
}