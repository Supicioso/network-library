﻿// ***********************************************************************
// <copyright file="ISoulPacketHandler.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Collections.Generic;
using SoulNetLib.Interfaces.Connection;
using SoulNetLib.Interfaces.Message;

namespace SoulNetLib.Interfaces.Protocol
{
    /// <summary>
    /// Interface ISoulPacketHandler
    /// </summary>
    public interface ISoulPacketHandler
    {
        /// <summary>
        /// Handles the specified message.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="message">The message.</param>
        void Handle(IConnectible connection, List<IMessage> message);
    }
}