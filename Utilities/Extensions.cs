﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using SoulNetLib.Interfaces.Message;

namespace SoulNetLib.Utilities
{
    /// <summary>
    /// A set of custom extensions.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Determines whether the specified socket is connected.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <returns><c>true</c> if the specified socket is connected; otherwise, <c>false</c>.</returns>
        public static bool IsConnected(this Socket socket)
        {
            try
            {
                var b1 = socket.Poll(1000, SelectMode.SelectRead);
                var b2 = socket.Poll(1000, SelectMode.SelectWrite);

                return b1 || b2 && socket.Connected;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Adds the data to the List, then adds on the number of filler bytes after the specified data, 
        /// with the option to specify the type of filler to add.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <param name="data">The data.</param>
        /// <param name="numFiller">The number filler bytes.</param>
        /// <param name="filler">The filler byte.</param>
        public static void AddFiller(this List<byte> list, IEnumerable<byte> data, int numFiller, byte filler = 0x00)
        {
            list.AddRange(data);
            for (int i = 0; i < numFiller; i++)
            {
                list.Add(filler);
            }
        }

        /// <summary>
        /// Trims the specified buffer.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="size">The size.</param>
        /// <returns>System.Byte[].</returns>
        public static byte[] Trim(this byte[] buffer, int size)
        {
            var buf = new byte[size];

            for (int i = 0; i < size; i++)
            {
                buf[i] = buffer[i];
            }

            return buffer = buf;
            return buffer = buffer.Take(size).ToArray();
        }

        /// <summary>
        /// Converts the byte array to a hexadecimal string.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string ToHex(this byte[] array, string delimiter = ":")
        {
            return BitConverter.ToString(array).Replace("-", delimiter);
        }

        /// <summary>
        /// Converts the message to a hexadecimal string.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string ToHex(this IMessage array, string delimiter = ":")
        {
            return BitConverter.ToString(array.Data).Replace("-", delimiter);
        }

        /// <summary>
        /// Formats a hexadecimal array into a string.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static string FormatHex(this byte[] data)
        {
            var builder = new StringBuilder(data.Length * 4);

            int count = 0, pass = 1;
            foreach (byte b in data)
            {
                if (count == 0)
                    builder.AppendFormat("{0,-6}\t", "[" + (pass - 1) * 16 + "]");

                count++;
                builder.Append(b.ToString("X2"));
                if (count == 4 || count == 8 || count == 12)
                    builder.Append(" ");

                if (count != 16) continue;
                builder.Append("\t");
                for (var i = (pass * count) - 16; i < (pass * count); i++)
                {
                    var c = (char)data[i];
                    if (c > 0x1f && c < 0x80)
                        builder.Append(c);
                    else
                        builder.Append(".");
                }
                builder.Append("\r\n");
                count = 0;
                pass++;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Converts a hex string into a byte array.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        public static byte[] ToBytes(this string data)
        {
            data = data.Replace(":", "");
            var array = new List<byte>();
            for (int i = 1; i < data.Length; i += 2)
            {
                string strByte = data[i - 1] + data[i].ToString();
                array.Add(Byte.Parse(strByte, NumberStyles.HexNumber));
            }
            return array.ToArray();
        }
    }
}