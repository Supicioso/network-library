﻿// ***********************************************************************
// <copyright file="AES.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SoulNetLib.Utilities.Encryption
{
    /// <summary>
    /// A Simple AES Encryption Class.
    /// </summary>
    public class AES
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AES" /> class.
        /// </summary>
        public AES()
        {
            Key = "qadfhb5rh7gf45k8jh6grd238h6k4b8h";
            IV = "a3df6g4b8d5m8g0z";
        }

        /// <summary>
        /// Gets the key.
        /// </summary>
        /// <value>The key.</value>
        private string Key { get; set; }

        /// <summary>
        /// Gets the iv.
        /// </summary>
        /// <value>The iv.</value>
        private string IV { get; set; }

        /// <summary>
        /// Encrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>System.Byte[].</returns>
        public byte[] Encrypt(byte[] data)
        {
            byte[] encData;

            using (var rijndael = new RijndaelManaged())
            {
                rijndael.Key = Encoding.UTF8.GetBytes(Key);
                rijndael.IV = Encoding.UTF8.GetBytes(IV);
                encData = EncryptBytes(rijndael, data);
            }

            return encData;
        }

        /// <summary>
        /// Decrypts the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>System.Byte[].</returns>
        public byte[] Decrypt(byte[] data)
        {
            byte[] decData;

            using (var rijndael = new RijndaelManaged())
            {
                rijndael.Key = Encoding.UTF8.GetBytes(Key);
                rijndael.IV = Encoding.UTF8.GetBytes(IV);
                decData = DecryptBytes(rijndael, data);
            }

            return decData;
        }

        /// <summary>
        /// Encrypts the bytes.
        /// </summary>
        /// <param name="alg">The alg.</param>
        /// <param name="message">The message.</param>
        /// <returns>System.Byte[].</returns>
        /// <exception cref="System.ArgumentNullException">alg</exception>
        private byte[] EncryptBytes(SymmetricAlgorithm alg, byte[] message)
        {
            if ((message == null) || (message.Length == 0))
            {
                return message;
            }

            if (alg == null)
            {
                throw new ArgumentNullException("alg");
            }

            using (var stream = new MemoryStream())
            using (var encryptor = alg.CreateEncryptor())
            using (var encrypt = new CryptoStream(stream, encryptor, CryptoStreamMode.Write))
            {
                encrypt.Write(message, 0, message.Length);
                encrypt.FlushFinalBlock();
                return stream.ToArray();
            }
        }

        /// <summary>
        /// Decrypts the bytes.
        /// </summary>
        /// <param name="alg">The alg.</param>
        /// <param name="message">The message.</param>
        /// <returns>System.Byte[].</returns>
        /// <exception cref="System.ArgumentNullException">alg</exception>
        private byte[] DecryptBytes(SymmetricAlgorithm alg, byte[] message)
        {
            if ((message == null) || (message.Length == 0))
            {
                return message;
            }

            if (alg == null)
            {
                throw new ArgumentNullException("alg");
            }

            using (var stream = new MemoryStream())
            using (var decryptor = alg.CreateDecryptor())
            using (var encrypt = new CryptoStream(stream, decryptor, CryptoStreamMode.Write))
            {
                encrypt.Write(message, 0, message.Length);
                encrypt.FlushFinalBlock();
                return stream.ToArray();
            }
        }
    }
}