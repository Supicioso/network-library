﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SoulNetLib.Utilities.Encryption;

namespace SoulNetLib.Utilities
{
    public class FileHandler : FileStream
    {
        public FileHandler(string file)
            : base(file, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)
        {
            AES = new AES();
        }

        private AES AES { get; set; }

        public bool UseEncryption { get; set; }

        public T Read<T>() where T : new()
        {
            var buf = new byte[1024];
            var buffer = new List<byte>();

            while (Read(buf, 0, buf.Length) > 0)
            {
                buffer.AddRange(buf);
            }

            var memBuf = new MemoryStream(buffer.ToArray());
            var fileData = new BinaryFormatter().Deserialize(memBuf);

            return (T)fileData;
        }

        public void Write(object data)
        {
            var buffer = data as byte[];
            var memBuf = new MemoryStream();

            if (buffer == null)
            {
                new BinaryFormatter().Serialize(memBuf, data);
                buffer = memBuf.ToArray();
            }

            Write(buffer, 0, buffer.Length);
        }
    }
}