﻿// ***********************************************************************
// <copyright file="Log.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.ComponentModel;
using SoulNetLib.Utilities;

namespace SoulNetLib.Common
{
    /// <summary>
    /// Interface ILog
    /// </summary>
    public interface ILog { }

    /// <summary>
    /// Class Log.
    /// </summary>
    public static class Log
    {
        /// <summary>
        /// Initializes static members of the <see cref="Log"/> class.
        /// </summary>
        static Log()
        {
            if (LogFile == null)
                LogFile = new FileHandler("log.txt");
        }

        /// <summary>
        /// Writes the information.
        /// </summary>
        /// <param name="log">The log.</param>
        /// <param name="text">The text.</param>
        public static void WriteInfo(this ILog log, string text)
        {
            var timeStamp = DateTime.Now.ToString("G tt");

            var str = string.Format("[{0}] {1}", log.GetType().Name, text);

            Console.WriteLine(str, Console.ForegroundColor = ConsoleColor.White);
        }

        /// <summary>
        /// Writes the notice.
        /// </summary>
        /// <param name="log">The log.</param>
        /// <param name="text">The text.</param>
        public static void WriteNotice(this ILog log, string text)
        {
            var str = string.Format("[{0}] {1}", log.GetType().Name, text);

            Console.WriteLine(str, Console.ForegroundColor = ConsoleColor.Yellow);
        }

        /// <summary>
        /// Writes the warning.
        /// </summary>
        /// <param name="log">The log.</param>
        /// <param name="text">The text.</param>
        public static void WriteWarning(this ILog log, string text)
        {
            var str = string.Format("[{0}] {1}", log.GetType().Name, text);

            Console.WriteLine(str, Console.ForegroundColor = ConsoleColor.DarkYellow);
        }

        /// <summary>
        /// Writes the error.
        /// </summary>
        /// <param name="log">The log.</param>
        /// <param name="text">The text.</param>
        public static void WriteError(this ILog log, string text)
        {
            var str = string.Format("[{0}] {1}", log.GetType().Name, text);

            Console.WriteLine(str, Console.ForegroundColor = ConsoleColor.Red);
        }

        /// <summary>
        /// Writes the error.
        /// </summary>
        /// <param name="log">The log.</param>
        /// <param name="e">The e.</param>
        public static void WriteError(this ILog log, Exception e)
        {
            var str = string.Format("[{0}] {1}", log.GetType().Name, e.Message);

            Console.WriteLine(str, Console.ForegroundColor = ConsoleColor.Red);
            Console.WriteLine(e.StackTrace, Console.ForegroundColor = ConsoleColor.Red);
        }

        /// <summary>
        /// Writes the debug.
        /// </summary>
        /// <param name="log">The log.</param>
        /// <param name="text">The text.</param>
        public static void WriteDebug(this ILog log, string text)
        {
            var str = string.Format("[{0}] {1}", log.GetType().Name, text);

            Console.WriteLine(str, Console.ForegroundColor = ConsoleColor.DarkRed);
        }

        /// <summary>
        /// Gets or sets the log file.
        /// </summary>
        /// <value>The log file.</value>
        public static FileHandler LogFile { get; set; }

        /// <summary>
        /// Gets or sets the log level.
        /// </summary>
        /// <value>The log level.</value>
        [DefaultValue(LogLevel.Warning)]
        public static LogLevel LogLevel { get; set; }
    }

    /// <summary>
    /// Enum LogLevel
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// The debug
        /// </summary>
        Debug = 5,
        /// <summary>
        /// The error
        /// </summary>
        Error = 4,
        /// <summary>
        /// The warning
        /// </summary>
        Warning = 3,
        /// <summary>
        /// The notice
        /// </summary>
        Notice = 2,
        /// <summary>
        /// The information
        /// </summary>
        Info = 1,
    }
}