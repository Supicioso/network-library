﻿// ***********************************************************************
// <copyright file="Connections.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Collections.Generic;
using SoulNetLib.Interfaces.Connection;

namespace SoulNetLib.Containers
{
    /// <summary>
    /// Class Connections.
    /// </summary>
    public class SoulConnections : List<IConnectible>
    {

    }
}