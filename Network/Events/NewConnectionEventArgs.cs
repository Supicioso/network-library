﻿// ***********************************************************************
// <copyright file="NewConnectionEventArgs.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Net.Sockets;
using SoulNetLib.Interfaces.Protocol;

namespace SoulNetLib.Network.Events
{
    /// <summary>
    /// Class NewConnectionEventArgs.
    /// </summary>
    public class NewConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the socket.
        /// </summary>
        /// <value>The socket.</value>
        public Socket Socket { get; set; }

        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        /// <value>The protocol.</value>
        public ISoulProtocol Protocol { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }
    }
}