﻿// ***********************************************************************
// <copyright file="MessageEventArgs.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using SoulNetLib.Interfaces.Connection;
using SoulNetLib.Interfaces.Message;

namespace SoulNetLib.Network.Events
{
    /// <summary>
    /// Class MessageEventArgs.
    /// </summary>
    public class MessageEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        /// <value>The messages.</value>
        public List<IMessage> Messages { get; set; }

        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        /// <value>The connection.</value>
        public IConnectible Connection { get; set; }
    }
}