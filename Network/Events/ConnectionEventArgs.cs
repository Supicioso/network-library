﻿// ***********************************************************************
// <copyright file="ConnectionEventArgs.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using SoulNetLib.Interfaces.Connection;

namespace SoulNetLib.Network.Events
{
    /// <summary>
    /// Class ConnectionEventArgs.
    /// </summary>
    public class ConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the connection.
        /// </summary>
        /// <value>The connection.</value>
        public IConnectible Connection { get; set; }

        /// <summary>
        /// Gets or sets the exception.
        /// </summary>
        /// <value>The exception.</value>
        public Exception Exception { get; set; }
    }
}