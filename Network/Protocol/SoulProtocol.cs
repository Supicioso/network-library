﻿// ***********************************************************************
// <copyright file="SoulProtocol.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Collections.Generic;
using System.Threading.Tasks;
using SoulNetLib.Interfaces.Message;
using SoulNetLib.Interfaces.Protocol;
using SoulNetLib.Network.Messages;

namespace SoulNetLib.Network.Protocol
{
    /// <summary>
    /// Class SoulProtocol.
    /// </summary>
    public class SoulProtocol : ISoulProtocol
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoulProtocol"/> class.
        /// </summary>
        public SoulProtocol()
        {
            ReceiveBufferSize = 4096;
        }

        /// <summary>
        /// Creates a list of messages from incoming data.
        /// </summary>
        /// <param name="receivedBytes">The received bytes.</param>
        /// <returns>A list of messages</returns>
        public async Task<List<IMessage>> CreateMessage(byte[] receivedBytes)
        {
            var message = new Message {Data = receivedBytes};
            await Task.FromResult(message);

            return new List<IMessage> { message };
        }

        /// <summary>
        /// Gets or sets the size of the receive buffer.
        /// </summary>
        /// <value>The size of the receive buffer.</value>
        public int ReceiveBufferSize { get; set; }

        /// <summary>
        /// Gets or sets the packet handler.
        /// </summary>
        /// <value>The packet handler.</value>
        public ISoulPacketHandler PacketHandler { get; set; }

        /// <summary>
        /// Gets or sets the protocol version.
        /// </summary>
        /// <value>The version.</value>
        public ProtocolVersion Version { get; set; }
    }

    public enum ProtocolVersion
    {
        V0 = 0,
        V1 = 1,
        V2 = 2,
        V3 = 3,
        V4 = 4,
        V5 = 5,
    }
}