﻿// ***********************************************************************
// <copyright file="SoulServer.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using SoulNetLib.Containers;
using SoulNetLib.Interfaces;
using SoulNetLib.Interfaces.Connection;
using SoulNetLib.Interfaces.Protocol;
using SoulNetLib.Network.Connections;
using SoulNetLib.Network.Events;

namespace SoulNetLib.Network
{
    /// <summary>
    /// Class SoulServer.
    /// </summary>
    public class SoulServer : SoulListener, ISoulServer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoulServer"/> class.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <param name="protocol">The protocol for parsing incoming data.</param>
        public SoulServer(int port, ISoulProtocol protocol)
            : base(IPAddress.Any, port)
        {
            ListenPort = port;

            SetHandlers(protocol);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoulServer"/> class.
        /// </summary>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="port">The port.</param>
        /// <param name="protocol">The protocol for parsing incoming data.</param>
        /// <exception cref="System.Exception">Invalid Listening Address!!</exception>
        public SoulServer(IPAddress ipAddress, int port, ISoulProtocol protocol)
            : base(ipAddress, port)
        {
            ListenPort = port;

            SetHandlers(protocol);
        }

        /// <summary>
        /// Sets the handlers.
        /// </summary>
        /// <param name="protocol">The protocol.</param>
        /// <param name="ipAddress">The listening address.</param>
        /// <exception cref="System.Exception">
        /// ISoulProtocol can't be null!
        /// or
        /// ISoulPacketHandler can't be null!
        /// </exception>
        private void SetHandlers(ISoulProtocol protocol)
        {
            if (protocol == null)
                throw new Exception("ISoulProtocol can't be null!");

            Protocol = protocol;

            Connections = new SoulConnections();
            
            ClientConnected = (sender, args) => { };
            ClientDisconnected = (sender, args) => { };
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public new void Start()
        {
            Start(5);

            Listening = true;

            Task.Run(() => Monitor());

            Task.Run(() => ListenLoop());
        }

        /// <summary>
        /// Loops the listener
        /// </summary>
        private void ListenLoop()
        {
            while (Listening)
            {
                var newClient = AcceptTcpClient();
                var socket = newClient.Client;

                Task.Run(() => InvokeNewSocket(socket));
            }
        }

        /// <summary>
        /// Invokes the new socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        private void InvokeNewSocket(Socket socket)
        {
            ConnectedClients++;
            
            ClientConnected.Invoke(this, new NewConnectionEventArgs
            {
                Socket = socket,
                Protocol = Protocol,
                Id = ConnectedClients
            });
        }

        /// <summary>
        /// Monitors this instance.
        /// </summary>
        /// <returns>Task.</returns>
        private async Task Monitor()
        {
            while (true)
            {
                try
                {
                    for (int i = 0; i < Connections.Count; i++)
                    {
                        var client = (SoulConnection) Connections[i];

                        if (client.Connected || client.State != ConnectionState.Disposed) continue;
                        
                        ClientDisconnected.Invoke(this, new ConnectionEventArgs { Connection = client });
                        Connections.Remove(client);

                        client.Dispose();
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message, e);
                }

                await Task.Delay(1);
            }
        }

        /// <summary>
        /// Gets or sets the protocol for incoming data.
        /// </summary>
        /// <value>The protocol.</value>
        public ISoulProtocol Protocol { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="SoulServer" /> is connected.
        /// </summary>
        /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
        public bool Connected { get; private set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the listen port.
        /// </summary>
        /// <value>The listen port.</value>
        public int ListenPort { get; set; }

        /// <summary>
        /// Gets or sets the number of connected clients.
        /// </summary>
        /// <value>The number of connected clients.</value>
        private int ConnectedClients { get; set; }

        /// <summary>
        /// Gets or sets the client.
        /// </summary>
        /// <value>The client.</value>
        public Socket Socket
        {
            get { return Server; } 
            set { Server = value; }
        }

        /// <summary>
        /// Gets the current connection state.
        /// </summary>
        /// <value>The state.</value>
        public ConnectionState State { get; private set; }

        /// <summary>
        /// Gets or sets the connections.
        /// </summary>
        /// <value>The connections.</value>
        public SoulConnections Connections { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SoulServer"/> is listening.
        /// </summary>
        /// <value><c>true</c> if listening; otherwise, <c>false</c>.</value>
        public bool Listening { get; set; }

        /// <summary>
        /// Occurs when a client connects.
        /// </summary>
        public event EventHandler<NewConnectionEventArgs> ClientConnected;

        /// <summary>
        /// Occurs when a client disconnects.
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ClientDisconnected;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Socket = null;
            Connections = null;
        }
    }
}