﻿// ***********************************************************************
// <copyright file="SoulConnection.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;
using SoulNetLib.Interfaces.Connection;
using SoulNetLib.Interfaces.Message;
using SoulNetLib.Interfaces.Protocol;
using SoulNetLib.Network.Events;
using SoulNetLib.Network.Messages;
using SoulNetLib.Utilities;

namespace SoulNetLib.Network.Connections
{
    /// <summary>
    /// Class SoulConnection.
    /// </summary>
    public abstract class SoulConnection : ISoulConnection, IMessaging
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoulConnection" /> class.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="protocol">The protocol.</param>
        protected SoulConnection(Socket socket, ISoulProtocol protocol)
        {
            Protocol = protocol;
            SetHandlers(socket);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoulConnection" /> class.
        /// </summary>
        /// <param name="protocol">The protocol.</param>
        protected SoulConnection(ISoulProtocol protocol)
        {
            Protocol = protocol;
            SetHandlers();
        }

        /// <summary>
        /// Sets the handlers.
        /// </summary>
        /// <param name="socket">The socket.</param>
        protected void SetHandlers(Socket socket = null)
        {
            State = ConnectionState.Connecting;

            Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            if (socket != null)
                Socket = socket;

            State = ConnectionState.Connected;

            ConnectedEvent = (sender, args) => { };

            ConnectedEvent.Invoke(this, new ConnectionEventArgs { Connection = this });

            Task.Run(() => ReadMessages());
        }

        /// <summary>
        /// Sends the specified data.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>System.Threading.Tasks.Task.</returns>
        public void Send(byte[] data)
        {
            try
            {
                Socket.Send(data);

                if (MessageSentEvent == null) 
                    throw new Exception("MessageSentEvent can't be null!");

                MessageSentEvent.Invoke(this, new MessageEventArgs {Connection = this, Messages = new List<IMessage> {new Message {Data = data}}});
            }
            catch (Exception e)
            {
                DisconnectedEvent.Invoke(this, new ConnectionEventArgs{Exception = e});

                Dispose();
            }
        }

        /// <summary>
        /// Reads incoming messages.
        /// </summary>
        private void ReadMessages()
        {
            while(!Connected) System.Threading.Thread.Sleep(1000);

            try
            {
                System.Threading.Thread.Sleep(1500);

                while (Connected)
                {
                    var messages = Protocol.CreateMessage(Receive()).Result;

                    if (MessageReceivedEvent == null)
                        throw new Exception("MessageReceivedEvent can't be null!");

                    var eventArgs = new MessageEventArgs {Messages = messages};
                    MessageReceivedEvent.Invoke(this, eventArgs);

                    State = ConnectionState.Connected;
                }
            }
            catch (Exception e)
            {
                DisconnectedEvent.Invoke(this, new ConnectionEventArgs { Exception = e });
            }
        }

        /// <summary>
        /// Receives data from the connected socket.
        /// </summary>
        /// <returns>System.Byte[].</returns>
        private byte[] Receive()
        {
            var buffer = new byte[Protocol.ReceiveBufferSize];

            buffer = Socket.Connected ? buffer.Trim(Socket.Receive(buffer)) : new byte[0];
            
            return buffer.Length == 0 ? null : buffer;
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="SoulConnection" /> is connected.
        /// </summary>
        /// <value><c>true</c> if connected; otherwise, <c>false</c>.</value>
        public bool Connected
        {
            get { return Socket.IsConnected(); }
        }

        /// <summary>
        /// Gets or sets the client socket.
        /// </summary>
        /// <value>The client socket.</value>
        public Socket Socket { get; set; }

        /// <summary>
        /// Gets the current connection state.
        /// </summary>
        /// <value>The state.</value>
        public ConnectionState State { get; set; }

        /// <summary>
        /// Gets or sets the protocol for incoming data.
        /// </summary>
        /// <value>The protocol.</value>
        public ISoulProtocol Protocol { get; set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            State = ConnectionState.Disconnecting;

            if (Socket != null)
                Socket.Dispose();

            Protocol = null;

            State = ConnectionState.Disposed;
        }

        #region EventHandlers

        /// <summary>
        /// Gets or sets the client connected event.
        /// </summary>
        /// <value>The client connected.</value>
        public event EventHandler<ConnectionEventArgs> ConnectedEvent;

        /// <summary>
        /// Gets or sets the client disconnected event.
        /// </summary>
        /// <value>The client disconnected.</value>
        public event EventHandler<ConnectionEventArgs> DisconnectedEvent;

        /// <summary>
        /// Occurs when a message is received.
        /// </summary>
        public event EventHandler<MessageEventArgs> MessageReceivedEvent;

        /// <summary>
        /// Occurs when a message is sent.
        /// </summary>
        public event EventHandler<MessageEventArgs> MessageSentEvent;

        #endregion
    }
}