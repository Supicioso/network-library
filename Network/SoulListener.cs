﻿// ***********************************************************************
// <copyright file="SoulListener.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Net;
using System.Net.Sockets;
using SoulNetLib.Interfaces;

namespace SoulNetLib.Network
{
    /// <summary>
    /// Class SoulListener.
    /// </summary>
    public class SoulListener : TcpListener, ISoulListener
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoulListener"/> class.
        /// </summary>
        /// <param name="localEp">The local ep.</param>
        public SoulListener(IPEndPoint localEp) : base(localEp)
        {
            LocalAddress = localEp.Address;
            LocalPort = localEp.Port;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Net.Sockets.TcpListener" /> class that listens for incoming connection attempts on the specified local IP address and port number.
        /// </summary>
        /// <param name="localaddr">An <see cref="T:System.Net.IPAddress" /> that represents the local IP address.</param>
        /// <param name="port">The port on which to listen for incoming connection attempts.</param>
        protected SoulListener(IPAddress localaddr, int port) : base(localaddr, port)
        {
            LocalAddress = localaddr;
            LocalPort = port;
        }

        /// <summary>
        /// Gets or sets the local address.
        /// </summary>
        /// <value>The local address.</value>
        public IPAddress LocalAddress { get; set; }

        /// <summary>
        /// Gets or sets the local port.
        /// </summary>
        /// <value>The local port.</value>
        public Int32 LocalPort { get; set; }

        /// <summary>
        /// Gets the underlying network <see cref="T:System.Net.Sockets.Socket" />.
        /// </summary>
        /// <value>The server.</value>
        protected new Socket Server { get; set; }
    }
}