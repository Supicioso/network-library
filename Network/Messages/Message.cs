﻿using SoulNetLib.Interfaces.Message;

namespace SoulNetLib.Network.Messages
{
    public class Message : IMessage
    {
        public int Id { get; set; }
        public byte[] Data { get; set; }
    }
}