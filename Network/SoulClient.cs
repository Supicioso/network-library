﻿// ***********************************************************************
// <copyright file="SoulClient.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Net;
using System.Net.Sockets;
using SoulNetLib.Interfaces.Message;
using SoulNetLib.Interfaces.Protocol;
using SoulNetLib.Network.Connections;

namespace SoulNetLib.Network
{
    /// <summary>
    /// Class SoulClient.
    /// </summary>
    public class SoulClient : SoulConnection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SoulConnection" /> class.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="protocol">The protocol.</param>
        public SoulClient(Socket socket, ISoulProtocol protocol) : base(socket, protocol)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SoulClient"/> class.
        /// </summary>
        public SoulClient(ISoulProtocol protocol) : base(protocol)
        {
            Protocol = protocol;
        }

        /// <summary>
        /// Connects to the specified server.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="port">The port.</param>
        /// <returns>System.Threading.Tasks.Task.</returns>
        public void Connect(string server, int port)
        {
            Socket.Connect(IPAddress.Parse(server), port);

            SetHandlers(Socket);
        }

        /// <summary>
        /// Sends the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>Task.</returns>
        public void Send(IMessage message)
        {
            Send(message.Data);
        }
    }
}