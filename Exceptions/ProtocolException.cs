﻿// ***********************************************************************
// <copyright file="ProtocolException.cs" company="Otoru Studios">
//     Copyright (c) Otoru Studios. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace SoulNetLib.Exceptions
{
    /// <summary>
    /// Class ProtocolException.
    /// </summary>
    public class ProtocolException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolException" /> class with a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ProtocolException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="e">The e.</param>
        public ProtocolException(string message, ProtocolException e) : base(message, e)
        {

        }
    }
}