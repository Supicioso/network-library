﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;
using SoulNetLib.Interfaces.Connection;
using SoulNetLib.Interfaces.Message;
using SoulNetLib.Interfaces.Protocol;
using SoulNetLib.Network;
using SoulNetLib.Network.Connections;
using SoulNetLib.Network.Events;
using SoulNetLib.Network.Protocol;
using SoulNetLib.Utilities;

namespace Server
{
    class Program
    {
        public static SoulServer Server { get; set; }

        public static List<object> LocalClients { get; set; }

        public static List<SoulConnection> RemoteClients { get; set; }

        static void Main(string[] args)
        {
            LocalClients = new List<object>();
            RemoteClients = new List<SoulConnection>();

            var protocol = new SoulProtocol {PacketHandler = new SoulPacketHandler()};

            Server = new SoulServer(1111, protocol);
            Server.ClientConnected += OnClientConnected;
            Server.Start();

            Task.Run(() => Client());

            Console.ReadLine();
        }

        private static void Client()
        {
            for (int i = 0; i < 1; i++)
            {
                var client = new SoulClient(new SoulProtocol { PacketHandler = new SoulPacketHandler() });
                client.DisconnectedEvent += OnDisconnectedEvent;
                client.MessageReceivedEvent += OnMessageReceivedEvent;
                client.MessageSentEvent += OnMessageSentEvent;

                client.Connect("72.219.11.200", 1111);

                client.Send(BitConverter.GetBytes(new Random().Next()));

                LocalClients.Add(client);
            }
        }

        private static void OnClientConnected(object sender, NewConnectionEventArgs e)
        {
            var client = new Connection(e.Socket, e.Protocol) { Id = e.Id };

            client.DisconnectedEvent += OnDisconnectedEvent;
            client.MessageReceivedEvent += OnMessageReceivedEvent;
            client.MessageSentEvent += OnMessageSentEvent;

            RemoteClients.Add(client);
        }

        private static void OnDisconnectedEvent(object sender, ConnectionEventArgs e)
        {
            if(e.Exception != null)
                throw new Exception(e.Exception.Message, e.Exception);
        }

        private static void OnMessageReceivedEvent(object sender, MessageEventArgs e)
        {
            var client = (IConnectible) sender;
            foreach (var c in RemoteClients)
            {
                c.Send(e.Messages[0].Data);
            }
            Console.WriteLine(client.Socket.LocalEndPoint + " - In " + InCount++ + " - " + e.Messages[0].Data.ToHex());
        }

        private static void OnMessageSentEvent(object sender, MessageEventArgs e)
        {
            Console.WriteLine(e.Connection.Socket.LocalEndPoint + " - Out " + OutCount++ + " - " + e.Messages[0].Data.ToHex());
        }

        static int InCount { get; set; }
        static int OutCount { get; set; }
    }

    public class SoulPacketHandler : ISoulPacketHandler
    {
        public void Handle(IConnectible connection, List<IMessage> message)
        {

        }
    }

    public class Connection : SoulConnection
    {
        public Connection(Socket socket, ISoulProtocol protocol)
            : base(socket, protocol)
        {

        }

        public Connection(ISoulProtocol protocol)
            : base(protocol)
        {
            Protocol = protocol;
        }
    }
}
